FROM hayaomunesato/ubuntu_mysql8

WORKDIR /application

RUN apt-get update


RUN apt-get install -y python-software-properties software-properties-common python3-software-properties

#preparing environment
RUN apt-get install locales apt-transport-https -y --no-install-recommends

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8

RUN add-apt-repository ppa:ondrej/php

RUN apt-get update

RUN apt-get install -y php7.2 php7.2-mysql git mysql-client

