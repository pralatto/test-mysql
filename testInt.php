<?php

include('vendor/autoload.php');

$concurrency = (int) ($_SERVER['argv'][1] ?? 0);

if ($concurrency < 1) {
    echo 'concurrency has to be grater than 0'; exit;
}

$data = include 'dbdata.php';

$test = new Test($data['host'], $data['user'], $data['password']);

//$test->startInt(30000000 / $concurrency);
$test->startIntPlus(30000000 / $concurrency);