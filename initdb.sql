

CREATE SCHEMA `testsql` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE `testsql`.`testInt` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `str1` VARCHAR(45) NULL,
  `str2` LONGTEXT NULL,
  `str3` VARCHAR(255) NULL,
  `str4` CHAR(5) NULL,
  `num1` TINYINT(1) NULL,
  `num2` INT(2) NULL,
  `num3` DECIMAL(10,2) NULL,
  `num4` FLOAT NULL,
  `num5` DOUBLE NULL,
  `createdAt` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `testsql`.`testUUID` (
  `id` VARCHAR(36) NOT NULL,
  `str1` VARCHAR(45) NULL,
  `str2` LONGTEXT NULL,
  `str3` VARCHAR(255) NULL,
  `str4` CHAR(5) NULL,
  `num1` TINYINT(1) NULL,
  `num2` INT(2) NULL,
  `num3` DECIMAL(10,2) NULL,
  `num4` FLOAT NULL,
  `num5` DOUBLE NULL,
  `createdAt` DATETIME NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `testsql`.`testIntPlus` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `key2` VARCHAR(36) NULL,
  `str2` LONGTEXT NULL,
  `str3` VARCHAR(264) NULL,
  `str4` CHAR(5) NULL,
  `num1` TINYINT(1) NULL,
  `num2` INT(2) NULL,
  `num3` DECIMAL(10,2) NULL,
  `num4` FLOAT NULL,
  `num5` DOUBLE NULL,
  `createdAt` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique1` (`key2`));