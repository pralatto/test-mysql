#!/usr/bin/env bash

CNT=$1

if [[ ${CNT} -eq "" ]]
then
echo "set concurrency"
exit
fi
php clearUUID.php
rm -rf /var/log/test-uuid-log-*.log

for ((i=0; i < CNT; i++))
do
setsid php testUUID.php ${CNT} > /var/log/test-uuid-log-${i}.log & > /dev/null
echo "> /var/log/test-uuid-log-${i}.log"
done

tail -f /var/log/test-uuid-log-*.log