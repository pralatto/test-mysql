<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
class Test
{

    private $connection;

    private $startTime = 0;

    public function __construct(string $host, string $user, string $password)
    {
        $this->connection = new mysqli($host, $user, $password);
        if ($this->connection->connect_errno) {
            die(sprintf("Connect failed: %s\n",  $this->connection->connect_error));
        }
        $this->connection->query('use testsql');
    }

    public function startUUID(int $cnt)
    {
        $this->startTime = time();
        echo 'start ', $cnt, "\n";
        $diff = floor($cnt / 1000);
        for ($i=0; $i<$cnt; $i++) {
            if ($i % $diff === 1) {
                $this->queryUUIDMeasure($i);
                continue;
            }

            $this->query($this->getQueryUUID());
        }
        echo 'done.';
    }

    private function queryUUIDMeasure(int $i)
    {
        $sql = $this->getQueryUUID();
        $start = (int)(microtime(true) * 10000);
        $this->query($sql);
        $now = (int)(microtime(true) * 10000);
        $time = $now - $start;
        $text = sprintf("%s,%s,%s\n",
            $time,
            $i,
            time() - $this->startTime
        );
        file_put_contents('./query-time-uuid.log', $text , FILE_APPEND);
        echo $text;
    }

    private function query(string $sql)
    {
        try {
            $result = $this->connection->query($sql);
            if (!$result) {
                echo $sql;
                throw new \Exception($this->connection->error);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    private function getQueryUUID(): string
    {
        return sprintf("INSERT INTO testUUID VALUES ('%s', '%s', '%s', '%s', '%s', %s, %s, %s, %s, %s, %s)",
            \Ramsey\Uuid\Uuid::uuid4()->toString(),
            $this->randomString(45),
            $this->randomString(100),
            $this->randomString(100),
            $this->randomString(5),
            rand(0,1),
            rand(10,99),
            rand(1000,9999) / 100,
            rand(1000,9999) / 100,
            rand(1000,9999) / 100,
            'NOW()'
        );
    }
    public function clearUUID()
    {
        $this->connection->query('TRUNCATE TABLE testUUID');
        if (copy('./query-time-uuid.log', './query-time-uuid.'.time().'.log')) {
            unlink('./query-time-uuid.log');
        }
    }


    private function randomString($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $pieces = [];
        $max = strlen($keyspace) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    public function startInt(int $cnt)
    {
        $this->startTime = time();
        echo 'start ', $cnt, "\n";
        $diff = floor($cnt / 1000);
        for ($i=0; $i<$cnt; $i++) {
            if ($i % $diff === 1) {
                $this->queryIntMeasure($i);
                continue;
            }

            $this->query($this->getQueryInt());
        }
        echo 'done.';
    }

    private function queryIntMeasure(int $i)
    {
        $sql = $this->getQueryInt();
        $start = (int)(microtime(true) * 10000);
        $this->query($sql);
        $now = (int)(microtime(true) * 10000);
        $time = $now - $start;
        $text = sprintf("%s,%s,%s\n",
            $time,
            $i,
            time() - $this->startTime
        );
        file_put_contents('./query-time-int.log', $text , FILE_APPEND);
        echo $text;
    }

    public function clearInt()
    {
        $this->connection->query('TRUNCATE TABLE testInt');
        if (copy('./query-time-int.log', './query-time-int.'.time().'.log')) {
            unlink('./query-time-int.log');
        }
    }

    private function getQueryInt(): string
    {
        return sprintf("INSERT INTO testInt VALUES (null, '%s', '%s', '%s', '%s', %s, %s, %s, %s, %s, %s)",
                $this->randomString(45),
                $this->randomString(100),
                $this->randomString(100),
                $this->randomString(5),
                rand(0,1),
                rand(10,99),
                rand(1000,9999) / 100,
                rand(1000,9999) / 100,
                rand(1000,9999) / 100,
                'NOW()'
            );
    }

    public function startIntPlus(int $cnt)
    {
        $this->startTime = time();
        echo 'start ', $cnt, "\n";
        $diff = floor($cnt / 1000);
        for ($i=0; $i<$cnt; $i++) {
            if ($i % $diff === 1) {
                $this->queryIntPlusMeasure($i);
                continue;
            }

            $this->query($this->getQueryIntPlus());
        }
        echo 'done.';
    }


    private function queryIntPlusMeasure(int $i)
    {
        $sql = $this->getQueryIntPlus();
        $start = (int)(microtime(true) * 10000);
        $this->query($sql);
        $now = (int)(microtime(true) * 10000);
        $time = $now - $start;
        $text = sprintf("%s,%s,%s\n",
            $time,
            $i,
            time() - $this->startTime
        );
        file_put_contents('./query-time-int-plus.log', $text , FILE_APPEND);
        echo $text;
    }

    public function clearIntPlus()
    {
        $this->connection->query('TRUNCATE TABLE testInt');
        if (copy('./query-time-int-plus.log', './query-time-int-plus.'.time().'.log')) {
            unlink('./query-time-int-plus.log');
        }
    }


    private function getQueryIntPlus(): string
    {
        return sprintf("INSERT INTO testIntPlus VALUES (null, '%s', '%s', '%s', '%s', %s, %s, %s, %s, %s, %s)",
                \Ramsey\Uuid\Uuid::uuid4()->toString(),
                $this->randomString(109),
                $this->randomString(100),
                $this->randomString(5),
                rand(0,1),
                rand(10,99),
                rand(1000,9999) / 100,
                rand(1000,9999) / 100,
                rand(1000,9999) / 100,
                'NOW()'
            );
    }
}