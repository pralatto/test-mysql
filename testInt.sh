#!/usr/bin/env bash

CNT=$1

if [[ ${CNT} -eq "" ]]
then
echo "set concurrency"
exit
fi

rm -rf /var/log/test-int-log-*.log
php clearInt.php
for ((i=0; i < CNT; i++))
do
setsid php testInt.php ${CNT} > /var/log/test-int-log-${i}.log & > /dev/null
echo "> /var/log/test-int-log-${i}.log"
done

tail -f /var/log/test-int-log-*.log